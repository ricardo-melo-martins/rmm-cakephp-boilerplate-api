<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsFixture
 */
class ActorsFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'actor';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'actor_id' => 1,
                'first_name' => 'Lorem ipsum dolor sit amet',
                'last_name' => 'Lorem ipsum dolor sit amet',
                'last_update' => 1643980761,
                'created_at_user_id' => 1,
                'created_at' => 'Lorem ipsum dolor sit amet',
                'modified_at' => 'Lorem ipsum dolor sit amet',
                'modified_at_user_id' => 1,
                'status' => 'L',
            ],
        ];
        parent::init();
    }
}
