# RMM CakePHP Boilerplate API

Boilerplate pessoal para criação de aplicações, pocs e estudo da tecnologia.

- CakePHP Framework 4.3.3

## Requisitos

- Php >= 7.4
- Composer > 2.x

## Instalação

Para clonar o projeto

```bash
$ git clone git@gitlab.com:ricardo-melo-martins/rmm-cakephp-boilerplate-api.git
```

Instale os pacotes com o comando

```bash
$ composer install
```

Copie o arquivo `config/app_local.example.php` para `config/app_local.php`

Neste arquivo altere hash `'__SALT__'` para segurança uma hash aleatória com 32 caracteres
```
28. 'salt' => env('SECURITY_SALT', '__SALT__'),
```

Para executar o projeto rapidamente com servidor embutido do PHP

```bash
$ php -S 127.0.0.1:8080 -t webroot
```

Abra o navegador e digite a url
http://127.0.0.1:8080

## Referência

[CakePHP Documentação Oficial](https://book.cakephp.org/4/en/index.html)

## Licença

O RMM CakePHP Boilerplate API é de código aberto licenciado sob a [licença MIT](https://opensource.org/licenses/MIT).
