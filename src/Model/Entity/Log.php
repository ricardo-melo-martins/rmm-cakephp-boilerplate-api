<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Entity\AbstractEntity;

/**
 * Log Entity
 *
 * @property int $id
 * @property int|null $timestamp
 * @property string|null $priority_name
 * @property int|null $priority
 * @property string|resource|null $message
 * @property string|null $type
 * @property int|null $error_code
 * @property string|null $exception_class
 * @property string|null $file_path
 * @property int|null $line
 * @property string|null $called_function
 * @property string|resource|null $trace
 * @property string|null $ip_address
 * @property string|null $request_id
 * @property string|resource|null $extra
 */
class Log extends AbstractEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'timestamp' => true,
        'priority_name' => true,
        'priority' => true,
        'message' => true,
        'type' => true,
        'error_code' => true,
        'exception_class' => true,
        'file_path' => true,
        'line' => true,
        'called_function' => true,
        'trace' => true,
        'ip_address' => true,
        'request_id' => true,
        'extra' => true,
    ];
}
