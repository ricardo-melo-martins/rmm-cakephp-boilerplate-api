<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use App\Handlers\Exception\ApiException as Exception;

abstract class AbstractEntity extends Entity {

    public function getValidationErrors() {

        try {

            $message = [];

            foreach ($this->getErrors() as $campo => $regra) {
                foreach ($regra as $nome => $msg) {
                    $nomeSoLetras = preg_replace('/[^a-zA-Z0-9]+/', '', $nome);
                    $message[$campo][$nomeSoLetras] = $msg;
                }
            }
            return $message;

        } catch (Exception $e) {
            throw $e;
        }


    }

    public function getValidationErrorsString()
    {
        $errors = $this->getValidationErrors();

        $msg = [];

        foreach ($errors as $campo => $regra) {
            foreach ($regra as $message) {
                $msg[] = strtoupper($campo) . ': ' . nl2br($message);
            }
        }

        return join('<br>', $msg);
    }

}
