<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Entity\AbstractEntity;

/**
 * Actor Entity
 *
 * @property int $actor_id
 * @property string $first_name
 * @property string $last_name
 * @property \Cake\I18n\FrozenTime $last_update
 * @property int|null $created_at_user_id
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property int|null $modified_at_user_id
 * @property string|null $status
 */
class Actor extends AbstractEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'last_update' => true,
        'created_at_user_id' => true,
        'created_at' => true,
        'modified_at' => true,
        'modified_at_user_id' => true,
        'status' => true,
    ];
}
