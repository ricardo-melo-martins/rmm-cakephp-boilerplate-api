<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Entity\AbstractEntity;

/**
 * Request Entity
 *
 * @property int $id
 * @property string|null $timestamp
 * @property string|null $request
 * @property string|null $request_raw
 * @property string|null $request_body
 * @property string|null $request_method
 * @property string|null $request_clength
 * @property string|null $request_host
 * @property string|null $request_route_path
 * @property string|null $request_route_url
 * @property string|null $request_scheme
 * @property string|null $response
 * @property string|null $response_raw
 * @property string|null $response_body
 * @property int|null $response_code
 * @property string|null $response_phrase
 * @property string|null $response_length
 * @property string|null $response_protocol
 * @property string|null $api_operation_id
 * @property string|null $api_operation
 * @property string|null $api_path
 * @property string|null $startts
 * @property string|null $endts
 * @property float|null $response_time
 * @property string|null $api_params_id
 * @property string|null $api_user_ip
 * @property string|null $api_user_ua
 */
class Request extends AbstractEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'timestamp' => true,
        'request' => true,
        'request_raw' => true,
        'request_body' => true,
        'request_method' => true,
        'request_clength' => true,
        'request_host' => true,
        'request_route_path' => true,
        'request_route_url' => true,
        'request_scheme' => true,
        'response' => true,
        'response_raw' => true,
        'response_body' => true,
        'response_code' => true,
        'response_phrase' => true,
        'response_length' => true,
        'response_protocol' => true,
        'api_operation_id' => true,
        'api_operation' => true,
        'api_path' => true,
        'startts' => true,
        'endts' => true,
        'response_time' => true,
        'api_params_id' => true,
        'api_user_ip' => true,
        'api_user_ua' => true,
    ];
}
