<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use App\Model\Table\AbstractTable;
use Cake\Validation\Validator;

/**
 * Requests Model
 *
 * @method \App\Model\Entity\Request newEmptyEntity()
 * @method \App\Model\Entity\Request newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Request[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Request get($primaryKey, $options = [])
 * @method \App\Model\Entity\Request findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Request patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Request[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Request|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RequestsTable extends AbstractTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('requests');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('timestamp')
            ->allowEmptyString('timestamp');

        $validator
            ->scalar('request')
            ->allowEmptyString('request');

        $validator
            ->scalar('request_raw')
            ->allowEmptyString('request_raw');

        $validator
            ->scalar('request_body')
            ->allowEmptyString('request_body');

        $validator
            ->scalar('request_method')
            ->allowEmptyString('request_method');

        $validator
            ->scalar('request_clength')
            ->allowEmptyString('request_clength');

        $validator
            ->scalar('request_host')
            ->allowEmptyString('request_host');

        $validator
            ->scalar('request_route_path')
            ->allowEmptyString('request_route_path');

        $validator
            ->scalar('request_route_url')
            ->allowEmptyString('request_route_url');

        $validator
            ->scalar('request_scheme')
            ->allowEmptyString('request_scheme');

        $validator
            ->scalar('response')
            ->allowEmptyString('response');

        $validator
            ->scalar('response_raw')
            ->allowEmptyString('response_raw');

        $validator
            ->scalar('response_body')
            ->allowEmptyString('response_body');

        $validator
            ->integer('response_code')
            ->allowEmptyString('response_code');

        $validator
            ->scalar('response_phrase')
            ->allowEmptyString('response_phrase');

        $validator
            ->scalar('response_length')
            ->allowEmptyString('response_length');

        $validator
            ->scalar('response_protocol')
            ->allowEmptyString('response_protocol');

        $validator
            ->scalar('api_operation_id')
            ->allowEmptyString('api_operation_id');

        $validator
            ->scalar('api_operation')
            ->allowEmptyString('api_operation');

        $validator
            ->scalar('api_path')
            ->allowEmptyString('api_path');

        $validator
            ->scalar('startts')
            ->allowEmptyString('startts');

        $validator
            ->scalar('endts')
            ->allowEmptyString('endts');

        $validator
            ->numeric('response_time')
            ->allowEmptyString('response_time');

        $validator
            ->scalar('api_params_id')
            ->allowEmptyString('api_params_id');

        $validator
            ->scalar('api_user_ip')
            ->allowEmptyString('api_user_ip');

        $validator
            ->scalar('api_user_ua')
            ->allowEmptyString('api_user_ua');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'logs_requests';
    }
}
