<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\ORM\Entity;

abstract class AbstractTable extends Table {

    protected $primaryKey = 'id';
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if($this->getPrimaryKey())
        {
            $this->primaryKey  = $this->getPrimaryKey();
        }
    }

    /**
     * Padroniza valores comuns ao salvar entidades
     *
     * @return bool
     */
    public function beforeSave(Event $event, Entity $entity, $options)
    {

        $user_id = 1; // TODO:

        if($entity->isNew())
        {
            $entity->created_at = date('Y-m-d H:i:s');
            $entity->status = 1;

            if(empty($entity->created_at_user_id) && !empty($user_id)){
                $entity->created_at_user_id = $user_id;
            }
        }
        else {

            $entity->modified_at = date('Y-m-d H:i:s');

            if(!empty($user_id)){
                $entity->modified_at_user_id = $user_id;
            }
        }

        return true;
    }
    public function findById(int $id, array $options = []) {

        $tableData = $this->find()->where([
            $this->primaryKey=>$id
        ]);

        return $tableData;
    }

    public function getById(int $id, array $options = []) {

        return $this->get($id, $options);

    }

    public function getAll() {

        return $this->Model->find();
    }

    public function search(array $fields = [],
                            array $joins = [],
                            array $conditions = [],
                            array $group = [],
                            array $order = [],
                            int $limit = 50,
                            bool $hydrate = false) {

        return $this->find()
            ->select($fields)
            ->join($joins)
            ->where($conditions)
            ->group($group)
            ->order($order)
            ->limit($limit)
            ->hydrate($hydrate);
    }


    public function create( $payload_data ){

        $entity_data = $this->newEntity($payload_data);

        if (!$this->save($entity_data)) {
            $data['error'] = $entity_data->getValidationErrors();
            return $data;
        }

        $data['id'] = isset($entity_data->id) ? $entity_data->id : null;

        return $data;
    }
}
