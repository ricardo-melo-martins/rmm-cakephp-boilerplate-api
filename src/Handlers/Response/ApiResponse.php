<?php
namespace App\Handlers\Response;

use App\Handlers\Exception\ApiException;
use App\Handlers\Exception\HttpException;
use App\Handlers\Exception\NotFoundException;

// ApiException;
// BadRequestException;
// InternalErrorException;
// MethodNotAllowedException;
// NotFoundException;
// UnauthorizedException;

use Cake\Core\Exception\CakeException;


use Cake\Core\Configure;
use Cake\Log\Log;

class ApiResponse
{
    protected $request;

    protected $response;

    public function __construct($req, $res)
    {
        $this->request = $req;
        $this->response = $res;
    }

    /**
     * Estratégia de apresentação padronizada para respostas
     *
     * @param array|string|object $data
     * @param int $statusCode
     *
     * @return array
     */
    public function responseOK($data, $statusCode = 200, string $statusMessage = null)
    {

        if(!isset($data)){
            throw new \Exception("É necessário ter algum dado para apresentar respostas", 1);
        }

        $response = [];

        $response['code'] = $statusCode;
        if($statusMessage){
            $response['message'] = $statusMessage;
        }

        // se for uma instancia do Cake\ORM
        if ($data instanceof \Cake\ORM\Query
        || $data instanceof \Cake\ORM\ResultSet
        || $data instanceof \Cake\ORM\Entity) {

            // transforma em lista
            $response['data'] = $data->toArray();
        }

        if (is_array($data)){
            $response['data'] = $data;
        }

        if ($this->request->getAttribute('paging'))
        {
            $response['paging'] = current($this->request->getAttribute('paging'));
        }

        if(Configure::read('debug') && is_array($data) && isset($data['debug'])){
            $response['debug'] = $data['debug'];
        }

        return $response;
    }


    /**
     * Estratégia de apresentação padronizada para respostas
     *
     * @param array|string|object $data
     * @param int $statusCode
     *
     * @return array|void
     */
    public function responseError($data, int $statusCode = 200, array $formErrorValidations = [])
    {

        if(!isset($data)){
            throw new \Exception("É necessário ter algum dado para apresentar respostas", 1);
        }

        // se $data for string então é uma mensagem simples de erro
        // que deve levar em consideração status 200, por ex. quando não existir um registro e eu
        // preferir retornar 200 ao invés de 404
        if(is_string($data)){

            return $this->renderError($data, $statusCode, $formErrorValidations);
        }


        if($data instanceof ApiException
        || $data instanceof HttpException
        || $data instanceof NotFoundException)
        {
            // dd($data);
            Log::error($data->getMessage());

            return $this->renderError($data->getMessage(), $data->getCode(), [], null );

            //return $this->renderError('Ocorreu um erro interno.', 503, [], null, $data->getMessage());
        }
// dd($data->getCode());
        return $this->renderError($data->getMessage(), $data->getCode());

    }

    public function renderError(
        string $message,
        $errorCode = null,
        array $validations = [],
        string $errorSuggestion = null,
        $debugMessage = null) {

        $error = [
            'code' => $errorCode,
            'message' => $message,
        ];

        if(!empty($validations)){
            $error['error']['validations'] = $validations;
        }

        if(!empty($errorSuggestion)){
            $error['error']['suggestion'] = $errorSuggestion;
        }

        if(Configure::read('debug') && !empty($debugMessage)){
            $error['debug'] = $debugMessage;
        }

        return $error;
    }


    private function handleThrowableError(\Throwable $exception)
    {

        $message = sprintf(
            "[%s] %s (%s:%s)\n", // Keeping same message format
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine(),
            $exception->getTraceAsString()
        );

        return $message;
    }
}
