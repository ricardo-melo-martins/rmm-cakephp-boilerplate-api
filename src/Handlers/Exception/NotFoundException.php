<?php
declare(strict_types=1);

namespace App\Handlers\Exception;

use Throwable;

/**
 * 404 não encontrado
 * Represents an HTTP 404 error.
 */
class NotFoundException extends HttpException
{
    /**
     * @inheritDoc
     */
    protected $_defaultCode = 404;

    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Not Found' will be the message
     * @param int|null $code Status code, defaults to 404
     * @param \Throwable|null $previous The previous exception.
     */
    public function __construct(?string $message = null, ?int $code = null, ?Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'O servidor não consegue encontrar o recurso solicitado. ';
        }
        parent::__construct($message, $code, $previous);
    }
}
