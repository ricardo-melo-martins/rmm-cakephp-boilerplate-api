<?php
declare(strict_types=1);

namespace App\Handlers\Exception;

use Throwable;

/**
 * 401 não autorizado
 * Represents an HTTP 401 error.
 */
class UnauthorizedException extends HttpException
{
    /**
     * @inheritDoc
     */
    protected $_defaultCode = 401;

    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Unauthorized' will be the message
     * @param int|null $code Status code, defaults to 401
     * @param \Throwable|null $previous The previous exception.
     */
    public function __construct(?string $message = null, ?int $code = null, ?Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'Indica que a solicitação requer informações de autenticação do usuário. O cliente PODE repetir a solicitação com um campo de cabeçalho de autorização adequado.';
        }
        parent::__construct($message, $code, $previous);
    }
}
