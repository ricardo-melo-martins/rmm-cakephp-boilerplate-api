<?php
declare(strict_types=1);

namespace App\Handlers\Exception;

use Throwable;

/**
 * 500 Erro interno do servidor
 * Represents an HTTP 500 error.
 */
class InternalErrorException extends HttpException
{
    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Internal Server Error' will be the message
     * @param int|null $code Status code, defaults to 500
     * @param \Throwable|null $previous The previous exception.
     */
    public function __construct(?string $message = null, ?int $code = null, ?Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'O servidor encontrou uma condição inesperada que o impediu de atender à solicitação.';
        }
        parent::__construct($message, $code, $previous);
    }
}
