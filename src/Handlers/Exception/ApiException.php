<?php
declare(strict_types=1);

namespace App\Handlers\Exception;


use Throwable;

/**
 * 400 Solicitação Inválida
 * Represents an HTTP 400 error.
 */
class ApiException extends HttpException
{
    /**
     * @inheritDoc
     */
    protected $_defaultCode = 400;

    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Not Found' will be the message
     * @param int|null $code Status code, defaults to 404
     * @param \Throwable|null $previous The previous exception.
     */
    public function __construct(?string $message = null, ?int $code = null, ?Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'A solicitação não pôde ser compreendida pelo servidor devido à sintaxe incorreta. O cliente NÃO DEVE repetir o pedido sem modificações.';
        }
        parent::__construct($message, $code, $previous);
    }
}
