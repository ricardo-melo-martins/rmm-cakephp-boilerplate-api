<?php
declare(strict_types=1);

namespace App\Handlers\Exception;

use Throwable;

/**
 * 405 Método Não Permitido
 * Represents an HTTP 405 error.
 */
class MethodNotAllowedException extends HttpException
{
    /**
     * @inheritDoc
     */
    protected $_defaultCode = 405;

    /**
     * Constructor
     *
     * @param string|null $message If no message is given 'Method Not Allowed' will be the message
     * @param int|null $code Status code, defaults to 405
     * @param \Throwable|null $previous The previous exception.
     */
    public function __construct(?string $message = null, ?int $code = null, ?Throwable $previous = null)
    {
        if (empty($message)) {
            $message = 'O método HTTP de solicitação é conhecido pelo servidor, mas foi desabilitado e não pode ser usado para esse recurso.';
        }
        parent::__construct($message, $code, $previous);
    }
}
