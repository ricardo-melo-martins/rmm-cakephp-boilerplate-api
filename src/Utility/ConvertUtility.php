<?php
declare(strict_types=1);

namespace App\Utility;


class ConvertUtility {

    public static function convertToHoursMins($time, $format = '%d:%d') {
        if ($time) {
            settype($time, 'integer');
            if ($time < 1) {
                return;
            }
            $hours = floor($time/60);
            $minutes = $time%60;
            if ($format == '%d:%d') {
                return str_pad($hours, 2, '0', STR_PAD_LEFT).':'.str_pad($minutes, 2, '0', STR_PAD_LEFT);
            } else {
                return sprintf($format, $hours, $minutes);
            }
        } else {
            return "";
        }
    }

    public static function convertToHoursMinsSecs($time, $format = '%d:%d:%d') {
        $time = $time / 60;
        if ($time < 0.00027) {
            return;
        }
        $hours = floor($time);
        $minutesFloat = ($time - $hours)*60;
        $minutes = floor($minutesFloat);
        $seconds = floor(($minutesFloat - $minutes)*60);
        if ($format == '%d:%d:%d') {
            return str_pad($hours, 2, '0', STR_PAD_LEFT).':'.str_pad($minutes, 2, '0', STR_PAD_LEFT).':'.str_pad($seconds, 2, '0', STR_PAD_LEFT);
        } else {
            return sprintf($format, $hours, $minutes, $seconds);
        }
    }


    public static function timeToDecimal($time, $tipo = 'minutos') {
        $timeArr = explode(':', $time);
        if (!isset($timeArr[2])) $timeArr[2] = 0;
        switch($tipo) {
            case 'horas':
                $decTime = ($timeArr[0]) + ($timeArr[1]/60) + ($timeArr[2]/3600);
                break;
            case 'minutos':
                $decTime = ($timeArr[0]*60) + ($timeArr[1]) + ($timeArr[2]/60);
                break;
            case 'segundos':
                $decTime = ($timeArr[0]*3600) + ($timeArr[1]*60) + ($timeArr[2]);
                break;
            default:
                $decTime = ($timeArr[0]*60) + ($timeArr[1]) + ($timeArr[2]/60);
        }

        return $decTime;
    }

    public static function decimalToTime($decimal, $tipo = 'minutos', $exibe_segundos = true) {
        $horas = 0; $minutos = 0; $segundos = 0;
        switch($tipo) {
            case 'dias':
                $horas = $decimal*24;
                break;
            case 'horas':
                $horas = $decimal;
                break;
            case 'minutos':
                $horas = floor($decimal / 60);
                $minutos = $decimal % 60;
                break;
            case 'segundos':
                $horas = floor($decimal / 3600);
                $decimal = $decimal % 3600;
                $minutos = floor($decimal / 60);
                $segundos = $decimal % 60;
                break;
            default:
                $horas = $decimal;
        }
        $hora_formatada = ($horas<10?sprintf("%02s",$horas):$horas).":".sprintf("%02s",$minutos).($exibe_segundos?":".sprintf("%02s",$segundos):"" );
        return $hora_formatada;
    }



    public static function jsonToArray($data = null)
    {
        if(!is_null($data)) {
            $json = (array)json_decode($data);
            foreach ($json as $key => $value) {
                if(is_object($value)) {
                    $json[$key] = (array)$value;
                } else {
                    $json[$key] = $value;
                }
            }
            $data = $json;
        }
        return $data;
    }


    public static function convertObjectClass($object, $final_class) {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($final_class),
            $final_class,
            strstr(strstr(serialize($object), '"'), ':')
        ));
    }

}
