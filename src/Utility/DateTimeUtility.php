<?php
declare(strict_types=1);

namespace App\Utility;

class DatetimeUtility {

    /**
	 * @param $date
	 * @param $format
	 *
	 * @return false|string
	 */
	public static function getLanguageFormattedDate($date, $format) {
		return date($format, strtotime($date));
	}

    public static function age($date) {
        $age = date('Y') - $date;
        if (date('md') < date('md', strtotime($date))) {
            return $age - 1;
        }
        return $age;
    }

}
