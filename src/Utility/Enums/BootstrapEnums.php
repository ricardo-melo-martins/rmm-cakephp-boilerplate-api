<?php
declare(strict_types=1);

namespace App\Utility\Enums;

class BootstrapEnums
{
    const muted = "muted";
	const primary = "primary";
	const success = "success";
	const info = "info";
	const warning = "warning";
	const danger = "danger";
}
