<?php
declare(strict_types=1);

namespace App\Utility;

class CryptoUtility {

	public static function encrypt($strToEncrypt, $key) {
        return openssl_encrypt($strToEncrypt, "AES-128-ECB", $key);
	}

    public static function decrypt($strToDecrypt, $key) {
        return openssl_decrypt($strToDecrypt, "AES-128-ECB", $key);
    }
}
