<?php
declare(strict_types=1);

namespace App\Utility;

class FileUtility {

    public static function read( string $filename )
    {
        $handle = @fopen($filename, "r");

        if ($handle) {
            while(!feof($handle)) {
                $fgets = fgets($handle, 4096);

                if(!empty($fgets)){
                    yield trim($fgets);
                }
            }

            fclose($handle);
        }
    }


    public static function existsRemoteFile(string $url)
    {
        $fp = @fopen($url, "r");

        if ($fp !== false) {
            fclose($fp);
        }

        return($fp);
    }

}
