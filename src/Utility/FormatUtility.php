<?php
declare(strict_types=1);

namespace App\Utility;

class FormatUtility {

    public static function cpf( string $data )
    {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "$1.$2.$3-$4", $data);
    }

    public static function cnpj( string $data )
    {
        return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "$1.$2.$3/$4-$5", $data);
    }

    public static function plate( string $data, string $local = 'pt_BR' )
    {
        if($local == 'pt_BR'){
            return strtoupper(preg_replace("/(\w{3})(\-?)(\d{3,4})/", "$1-$3", str_replace(' ', '', $data)));
        }
        return $data;
    }

    public static function cep( string $data, string $local = 'pt_BR')
    {
        return substr($data, 0,5).' - '.substr($data, 5);
    }
    public static function telephone( string $data, string $local = 'pt_BR')
    {
        $number = preg_replace("/[^0-9]/i",'',$data);

        if (trim($number)=='') return '';

        if (strlen($number) <10) {
            if (strlen($number) <= 8) {
                return preg_replace("/(\d{4})(\d{4})/", "$1-$2", $number);
            } else {
                return preg_replace("/(\d{5})(\d{4})/", "$1-$2", $number);
            }
        } else {
            if (strlen($number) <= 10) {
                return preg_replace("/(\d{2})(\d{4})(\d{4})/", "($1) $2-$3", $number);
            } else {
                return preg_replace("/(\d{2})(\d{5})(\d{4})/", "($1) $2-$3", $number);
            }
        }

    }
}
