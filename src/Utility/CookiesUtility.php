<?php
declare(strict_types=1);

namespace App\Utility;

class CookiesUtility {

    /**
     * @param $cookie_name
     * @param string $empty_value
     * @return mixed|string
     */
    public static function getCookie($cookie_name, $empty_value="") {

        $cookie = $empty_value;

        if (isset($_COOKIE[$cookie_name])) {
            if ($_COOKIE[$cookie_name] != "") {
                $cookie = $_COOKIE[$cookie_name];
            }
        }

        return $cookie;
    }

    /**
     * @param $cookie_name
     * @param $cookie_value
     * @param int $expires
     */
    public static function setCookie($cookie_name, $cookie_value, $expires = 0) {
        setcookie($cookie_name, $cookie_value, $expires, "/");
    }

    /**
     * @param $cookie_name
     */
    public static function unsetCookie($cookie_name) {
        setcookie($cookie_name, "", 1, "/");
    }

}
