
<?php
declare(strict_types=1);

namespace App\Utility;

class StdUtility {

    public static function toBool($var) {
        if (!is_string($var)) return (bool) $var;
        switch (strtolower($var)) {
          case '1':
          case 'true':
          case 'sim':
          case 'on':
          case 'yes':
          case 'y':
            return true;
          default:
            return false;
        }
    }

    /**
     * limpa dados que não sejam alfa
     *
     * @param string $string
     * @return string | null
     */
    public static function toAlpha( $string ) {
        return filter_var($string, FILTER_SANITIZE_STRING);
    }

    /**
     * limpa dados que não sejam numericos
     *
     * @param string $string
     * @return string | null
     */
    public static function toNum( $string ) {
        return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * limpa dados que não sejam numericos
     *
     * @param string $string
     * @return string | null
     */
    public static function toInt( $string ) {
        return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
    }
}


