<?php

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;


class ResponseTimeMiddleware implements MiddlewareInterface {

    const HEADER = 'X-Response-Time';

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {

        $server = $request->getServerParams();

        $startTime = $server['REQUEST_TIME_FLOAT'] ?? microtime(true);

        $response = $handler->handle($request);

        return $response->withHeader(self::HEADER, sprintf('%2.3f', (microtime(true) - $startTime) * 1000));

    }

}
