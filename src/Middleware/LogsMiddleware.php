<?php
/**
 * TODO: gravar logs de forma async
 */
namespace App\Middleware;

// use Cake\Http\Cookie\Cookie;
// use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;

use App\Service\LogService;

class LogsMiddleware implements MiddlewareInterface {

    const HEADER = 'X-Response-Time';

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {

        $server = $request->getServerParams();

        $startTime = $server['REQUEST_TIME_FLOAT'] ?? microtime(true);

        $response = $handler->handle($request);

        $size = is_null($response->getBody()->getSize()) ? 0 : $response->getBody()->getSize();

        $responseTime = sprintf('%2.3f', (microtime(true) - $startTime) * 1000);

        (new LogService())->saveRequest($request, $response, $responseTime);

        return $response->withHeader(self::HEADER, sprintf('%2.3f', (microtime(true) - $startTime) * 1000))
                          ->withHeader('Content-Length', (string) $size);

    }

}
