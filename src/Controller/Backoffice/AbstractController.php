<?php
declare(strict_types=1);

namespace App\Controller\Backoffice;

use App\Controller\ApiController;
use App\Handlers\Exception\ApiException as Exception;

abstract class AbstractController extends ApiController
{

    protected $Table;

    public function initialize(): void
    {
        parent::initialize();

        $this->Table = $this->loadModel($this->modelClass);

    }

    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()|null|void
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $data = [];

        try {

            $data = $this->paginate($this->Table);

            return $this->responseOK($data);

        } catch (Exception $e) {

            return $this->responseError($e);
        }

    }

    /**
     * View method
     *
     * @param string|null $id id.
     * @return \App\Handlers\Response\ApiResponse|null|void
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function view(int $id = null)
    {
        try {

            $tableData = $this->Table->findById($id);

            if($tableData->count())
            {
                return $this->responseOK($tableData);
            }

            return $this->responseError("Não foi possível encontrar o registro solicitado", 404);

        } catch (Exception $e) {

            return $this->responseError($e);
        }
    }


    /**
     * Add method
     *
     * @return \App\Handlers\Response\ApiResponse|null|void Renders view
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function add()
    {
        $this->request->allowMethod(['POST']);

            $entity = $this->Table->newEmptyEntity();

            try {

                $entityData = $this->Table->patchEntity($entity, $this->getRequest()->getData());

                // se possui erros de formulário
                if ($entity->hasErrors()) {

                    return $this->responseError(
                        sprintf("Por favor, revise os dados fornecidos."),
                        204,
                        $entity->getErrors()
                    );
                }

                // se falhar
                if (!$this->Table->save($entityData))
                {
                    throw new Exception('Não foi possível gravar estes dados.');
                }

                // incluir o ID do registro na resposta
                $data = [$this->Table->getPrimaryKey() => $entityData->{$this->Table->getPrimaryKey()}];

                return $this->responseOK($data, 200, "Registro incluído com sucesso");

            } catch (Exception $e) {
                return $this->responseError($e);
            }
    }

    /**
     * Edit method
     *
     * @param string|null $id id.
     *
     * @return \App\Handlers\Response\ApiResponse|null|void
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function edit(int $id = null)
    {
        $this->request->allowMethod(['PUT']);


        $tableData = $this->Table->findById($id);

        // se não encontrar
        if(!$tableData->count())
        {
            return $this->responseError('Não foi possível encontrar o registro solicitado', 204);
        }

        $entityTableToPatch = $this->Table->patchEntity($this->Table->get($id), $this->request->getData());

        // verifica se ocorreu alguma mudança
        // senão ocorreu então retorna $entityTable já carregada
        if(empty($entityTableToPatch->getDirty()))
        {
            return $this->responseOK($entityTableToPatch, 304, 'Registro atualizado com sucesso');
        }

        if (!$this->Table->save($entityTableToPatch))
        {
            throw new Exception('Não foi possível gravar estes dados');
        }

        return $this->responseOK([], 201, 'Registro Atualizado com sucesso');
    }

    /**
     * Delete method
     *
     * @param string|null $id id.
     * @return \App\Handlers\Response\ApiResponse|null|void Renders view
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function delete(int $id = null)
    {

        $this->request->allowMethod(['delete']);

        $tableData = $this->Table->findById($id);

        if(!$tableData->count())
        {
            return $this->responseError('Não foi possível encontrar o registro solicitado', 204);
        }

        try {

            if ($this->Table->delete($this->Table->get($id))) {
                return $this->responseOK('Registro removido com sucesso');
            }

        } catch (Exception $e) {
            $response = $this->getResponse();
            $response = $response->withStatus(200);
            $this->setResponse($response);

            throw $e;
        }
    }

}
