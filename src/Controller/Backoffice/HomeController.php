<?php
declare(strict_types=1);

namespace App\Controller\Backoffice;

use App\Controller\ApiController;

/**
 * Home Controller
 *
 */
class HomeController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()
     */
    public function index()
    {
        $data = [];

        $this->request->allowMethod(['get']);

        return $this->responseOK($data);
    }

}
