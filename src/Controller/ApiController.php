<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;

use App\Handlers\Response\ApiResponse;

/**
 * Api Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class ApiController extends Controller
{
    /**
     * Initialization hook method.
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'checkHttpCache' => true
        ]);
    }


    /**
     * Before Render hook method.
     * @return void
     */
    public function beforeRender(EventInterface $event)
    {
        parent::beforeRender($event);

        $this->viewBuilder()->setOption('serialize', true);
        $this->viewBuilder()->enableAutoLayout(false);

    }

    /**
     * Estratégia de apresentação padronizada para respostas
     *
     * @param array|string|object $data
     * @param array $formErrorValidations
     *
     * @return void
     */
    public function responseOK($data, int $statusCode = 200, string $statusMessage = null)
    {
        $content = [];

        $content = (new ApiResponse(
            $this->getRequest(),
            $this->getResponse(),
            ))->responseOK($data, $statusCode, $statusMessage);

        $this->set($content);
    }

    public function responseError($data, int $statusCode = 200, array $formValidations = [])
    {
        $content = [];

        $content = (new ApiResponse(
            $this->getRequest(),
            $this->getResponse(),
            ))->responseError($data, $statusCode, $formValidations);

        $this->set($content);
    }
}
