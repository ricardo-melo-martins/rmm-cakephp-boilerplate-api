<?php
declare(strict_types = 1)
;

namespace App\Controller\Sakila;

use App\Controller\ApiController;
use App\Handlers\Exception\ApiException;

use Cake\Event\EventInterface;

use App\Handlers\Exception\BadRequestException;
use App\Handlers\Exception\NotFoundException;
use App\Handlers\Exception\InternalErrorException;

abstract class AbstractController extends ApiController
{

    protected $Table;

    public function initialize(): void
    {
        parent::initialize();

        $this->Table = $this->loadModel($this->modelClass);
        $this->loadComponent('ApiRequest');

    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

    }

    public function afterFilter(EventInterface $event)
    {
        parent::afterFilter($event);

    }

    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()|null|void
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        if (!$this->ApiRequest->isValid()) {
            // throw new BadRequestException();
            return $this->responseError(
                sprintf("Por favor, revise os dados fornecidos."),
                204,
                $this->ApiRequest->getErrors()
            );
        }

        $data = [];

        try {

            $tableData = $this->Table; //->searchAll($this->getRequest()->getQueryParams());

            $data = $this->paginate($tableData);

            return $this->responseOK($data);

        } catch (ApiException $e) {

            return $this->responseError($e);
        }

    }

    /**
     * View method
     *
     * @param string|null $id id.
     * @return \App\Handlers\Response\ApiResponse|null|void
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function view(int $id = null)
    {
        try {

            $tableData = $this->Table->findById($id);

            if($tableData->count())
            {
                throw new NotFoundException();
            }

            return $this->responseOK($tableData);

        } catch (ApiException $e) {

            return $this->responseError($e);
        }
    }


    /**
     * Add method
     *
     * @return \App\Handlers\Response\ApiResponse|null|void Renders view
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function add()
    {
        $this->request->allowMethod(['POST']);

            $entity = $this->Table->newEmptyEntity();

            try {

                $entityData = $this->Table->patchEntity($entity, $this->getRequest()->getData());

                // se possui erros de formulário
                if ($entity->hasErrors()) {

                    return $this->responseError(
                        sprintf("Por favor, revise os dados fornecidos."),
                        204,
                        $entity->getErrors()
                    );
                }

                // se falhar
                if (!$this->Table->save($entityData))
                {
                    throw new ApiException('Não foi possível gravar estes dados.');
                }

                // incluir o ID do registro na resposta
                $data = [$this->Table->getPrimaryKey() => $entityData->{$this->Table->getPrimaryKey()}];

                return $this->responseOK($data, 200, "Registro incluído com sucesso");

            } catch (ApiException $e) {
                return $this->responseError($e);
            }
    }

    /**
     * Edit method
     *
     * @param string|null $id id.
     *
     * @return \App\Handlers\Response\ApiResponse|null|void
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function edit(int $id = null)
    {
        $this->request->allowMethod(['PUT']);


        $tableData = $this->Table->findById($id);

        // se não encontrar
        if(!$tableData->count())
        {
            throw new NotFoundException();
        }

        $entityTableToPatch = $this->Table->patchEntity($this->Table->get($id), $this->request->getData());

        // verifica se ocorreu alguma mudança
        // senão ocorreu então retorna $entityTable já carregada
        if(empty($entityTableToPatch->getDirty()))
        {
            return $this->responseOK($entityTableToPatch, 304, 'Registro atualizado com sucesso');
        }

        try {

            if (!$this->Table->save($entityTableToPatch))
            {
                throw new ApiException('Não foi possível gravar estes dados');
            }

        } catch (ApiException $e) {
            return $this->responseError($e);
        }

        return $this->responseOK([], 201, 'Registro Atualizado com sucesso');
    }

    /**
     * Delete method
     *
     * @param string|null $id id.
     * @return \App\Handlers\Response\ApiResponse|null|void Renders view
     * @throws \App\Handlers\Exception\ApiException.
     */
    public function delete(int $id = null)
    {

        $this->request->allowMethod(['delete']);

        $tableData = $this->Table->findById($id);

        if(!$tableData->count())
        {
            throw new NotFoundException();
        }

        try {

            if ($this->Table->delete($this->Table->get($id)))
            {
                return $this->responseOK([], 200, 'Registro removido com sucesso');
            }

        } catch (ApiException $e) {
            return $this->responseError($e);
        }
    }


    // TODO: for PATCH endpoint
    public function updateList(int $id = null, $status = null)
    {

    }

    // TODO:
    public function updateStatus(int $id = null, $status = null)
    {

    }

}
