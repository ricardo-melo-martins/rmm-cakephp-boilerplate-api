<?php
declare(strict_types=1);

namespace App\Controller\App;

use App\Controller\ApiController;

/**
 * Notifications Controller
 *
 */
class NotificationsController extends ApiController
{
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()
     */
    public function index()
    {
        $data = [];

        $this->request->allowMethod(['get']);

        return $this->responseOK($data);
    }

}
