<?php

declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;

use Cake\Validation\Validator;
use Cake\Utility\Hash;
class ApiRequestComponent extends Component
{

    private $_whitelistArgs = [
        'page', 'limit', 'order', 'sort', 'direction', 'q', 'id', 'status', 'autocomplete'
    ];

    private $rulesAllowedAccepts = [
        'limit' => [
            'numeric' => [
                'rule' => 'numeric',
                'message' => 'Parâmetro inválido, deve ser numérico'
            ],
            'not-blank' => [
                'rule' => 'notBlank',
                'message' => 'Parâmetro inválido'
            ],
            'min-length' => [
                'rule' => ['minLength', 1],
                'message' => 'Parâmetro inválido'
            ],
            'max-length' => [
                'rule' => ['maxLength', 3],
                'message' => 'Parâmetro inválido'
            ]
        ],
        'page' => [
            'numeric' => [
                'rule' => 'numeric',
                'message' => 'Parâmetro inválido, deve ser numérico'
            ],
            'not-blank' => [
                'rule' => 'notBlank',
                'message' => 'Parâmetro inválido'
            ],
            'max-length' => [
                'rule' => ['maxLength', 3]
            ]
        ],
        'direction' => [
            'truthy' => [
                'rule' => [
                    'truthy', ['ASC','DESC'],
                    'message' => 'Parâmetro inválido'
                ]
            ],
            'not-blank' => [
                'rule' => 'notBlank',
                'message' => 'Parâmetro inválido'
            ],
            'max-length' => [
                'rule' => ['maxLength', 4]
            ]
        ],
        'name' => [
            'max-length'=> [
                'rule' => ['maxLength', 10]
            ],
            'min-length' => [
                'rule' => ['minLength', 2]
            ],
            'alpha-numeric' => [
                'rule' => 'AlphaNumeric',
                'message' => 'Parâmetro inválido, deve ser Alfa numérico'
            ]
        ],
        'status' => [
            'truthy' => [
                'rule' => ['truthy', ['1','0']],
                'message' => 'Parâmetro inválido'
            ]
        ],
        'id' => [
            'numeric' => [
                'rule' => 'numeric',
                'message' => 'Parâmetro inválido, deve ser numérico'
            ],
            'not-blank' => [
                'rule' => 'notBlank',
                'message' => 'Parâmetro inválido'
            ],
            'max-length' => [
                'rule' => ['maxLength', 8]
            ]
        ],
        'autocomplete' => [],
        'q' => [
            'not-blank' => [
                'rule' => 'notBlank',
                'message' => 'Parâmetro inválido'
            ],
            'max-length' => [
                'rule' => ['maxLength', 3],
                'last' => true,
                'message' => 'Parâmetro inválido'
            ],
            'alpha-numeric' => [
                'rule' => 'AlphaNumeric',
                'message' => 'Parâmetro inválido, deve ser Alfa numérico'
            ]
        ],
    ];
    public $_errors = [];

    protected $_query_params = [];

        // inicializa o componente
    public function initialize(array $config): void
    {
        $this->_config = $config;

        $this->_validator = new Validator();
        $this->_controller = $this->getController();
        $this->_request = $this->_controller->getRequest();

        $this->_action = $this->_request->getParam('action');
        $this->_method = $this->_request->getEnv('ORIGINAL_REQUEST_METHOD');
        $this->_isGET = $this->_request->is('get');
        $this->_isPOST = $this->_request->is('post');
        $this->_isPUT = $this->_request->is('put');
        $this->_isPATCH = $this->_request->is('patch');
        $this->_isDELETE = $this->_request->is('delete');
    }

    public function isValid()
    {
        $queryParams = $this->getController()->getRequest()->getQueryParams();

        $this->_query_params = $this->cleanParams($queryParams);

        $resultValidation = $this->validateQueryParams($queryParams);

        if(!empty($resultValidation)){

            $this->_errors = $resultValidation;

            return false;
        }

        return true;
    }

    public function cleanParams($arrQueryParams)
    {
        $valid = [];

        foreach ($this->_whitelistArgs as $key => $value) {

            if(isset($arrQueryParams[$value]))
            {
                $valid = Hash::merge($valid, [$value => $arrQueryParams[$value]]);
            }
        }

        return $valid;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function getQueryParams()
    {
        return $this->_query_params;
    }

    public function validateQueryParams(array $arrParams)
    {

        foreach ($this->rulesAllowedAccepts as $param => $rules) {
            foreach ($rules as $keyRule => $rule) {
                if(!is_array($rule)){
                   // throw new Exception();
                }

                $this->_validator->add($param, $keyRule, $rule);
            }
        }

        return $this->_validator->validate($arrParams);
    }
}
