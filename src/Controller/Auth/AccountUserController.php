<?php
declare(strict_types=1);

namespace App\Controller\Auth;

use App\Controller\ApiController;

/**
 * AccountUser Controller
 *
 */
class AccountUserController extends ApiController
{

    /**
     * RefreshToken
     *
     *
     *
     * @return App\Controller\ApiController\response()
     */
    public function refreshToken( )
    {
        $this->request->allowMethod(['post']);

        $data = [];

        return $this->responseOK($data);
    }

    /**
     * VerifyEmail
     *
     *
     *
     * @return App\Controller\ApiController\response()
     */
    public function verifyEmail( )
    {
        $this->request->allowMethod(['post']);

        $data = [];

        return $this->responseOK($data);
    }

    /**
     * ForgotPassword
     *
     *
     *
     * @return App\Controller\ApiController\response()
     */
    public function forgotPassword( )
    {
        $this->request->allowMethod(['post']);

        $data = [];

        return $this->responseOK($data);
    }

    /**
     * ResetPassword
     *
     *
     *
     * @return App\Controller\ApiController\response()
     */
    public function resetPassword( )
    {
        $this->request->allowMethod(['post']);

        $data = [];

        return $this->responseOK($data);
    }


    /**
     * Info
     *
     * Recuperar credenciais de acesso de uma conta
     *
     * @return App\Controller\ApiController\response()
     */
    public function info( )
    {
        $this->request->allowMethod(['post']);

        $data = [];

        return $this->responseOK($data);
    }

}
