<?php
declare(strict_types=1);

namespace App\Controller\Auth;

use App\Controller\ApiController;

/**
 * SignIn Controller
 *
 */
class SignInController extends ApiController
{
    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()
     */
    public function login()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData();

        return $this->responseOK($data);
    }


    /**
     * SignOut method
     *
     * @return App\Controller\ApiController\response()
     */
    public function logout()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData();

        return $this->responseOK($data);
    }



}
