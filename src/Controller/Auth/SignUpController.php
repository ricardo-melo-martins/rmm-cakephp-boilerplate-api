<?php
declare(strict_types=1);

namespace App\Controller\Auth;

use App\Controller\ApiController;

/**
 * SignUp Controller
 *
 */
class SignUpController extends ApiController
{
    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()
     */
    public function register()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData();

        return $this->responseOK($data);
    }

}
