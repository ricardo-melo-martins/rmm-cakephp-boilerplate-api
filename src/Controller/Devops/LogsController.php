<?php
declare(strict_types=1);

namespace App\Controller\Devops;

use App\Controller\ApiController;
use App\Service\LogCakeService;

use Cake\Core\Configure;
use App\Handlers\Exception\ApiException;

class LogsController extends ApiController
{

    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $data = [];

        try {

            if(Configure::read('debug')) {
                $data = (new LogCakeService())->getLogs();
            }

        } catch (ApiException $e) {
            return $this->responseError($e);
        }

        return $this->responseOK($data);
    }

}
