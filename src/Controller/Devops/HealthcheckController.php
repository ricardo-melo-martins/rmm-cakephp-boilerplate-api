<?php
declare(strict_types=1);

namespace App\Controller\Devops;

use App\Controller\ApiController;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;

class HealthcheckController extends ApiController
{

    /**
     * Index method
     *
     * @return App\Controller\ApiController\response()
     */
    public function index()
    {
        $this->request->allowMethod(['get']);

        $data = [
            'api' =>  Configure::read('api.name' ),
            'version' => Configure::read('api.version'),
            'timestamp' => FrozenTime::now(),
        ];

        return $this->responseOK($data);
    }

}
