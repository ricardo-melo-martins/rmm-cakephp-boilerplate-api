<?php
namespace App\Service;

use App\Service\AbstractService;
use App\Utility\FileUtility as File;

use Cake\Collection\Collection;
use App\Utility\ValidUtility as Validate;

use App\Handlers\Exception\ApiException as Exception;
class LogCakeService extends AbstractService {

    const LOG_FILE_ERROR = LOGS . "error.log";

    public function getLogs( $options =  [
        'trace' => false,
        'request'=> true,
        'raw'=>false,
        'attributes'=> true,
        'referer'=>false
    ])
    {

        try {

            $log = $this->readLog( $options );

            $collection = new Collection($log);

            return $collection->sortBy('date', SORT_DESC, SORT_NATURAL)->toList();

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function readLog( array $options = [])
    {

        if(!file_exists(self::LOG_FILE_ERROR)){
            throw new Exception(sprintf('Arquivo %s não encontrado.', self::LOG_FILE_ERROR), 1);
        }

        $file_serialized = [];

        $fp = @fopen(self::LOG_FILE_ERROR, "r");

        if ($fp) {

            $serialized = [];

            while (($buffer = fgets($fp, 4096)) !== false) {

                if (Validate::isJson($buffer)) {
                    $serialized = $this->parseJsonMessageLog($buffer, $options);
                    $file_serialized[] = $serialized;
                }
            }

            if (!feof($fp)) {
                throw new Exception(sprintf('Unexpected fgets() fail file %s.', self::LOG_FILE_ERROR), 1);
            }

            fclose($fp);
        }

        return $file_serialized;

    }
    public function parseJsonMessageLog( $message, array $options = [] )
    {
        $serialized = [];

        $log = json_decode($message);

        $message = $log->message;
        $serialized['date'] = $log->date;
        $serialized['level'] = $log->level;

        //Sets exception type and message
        if(preg_match('/^(\[([^\]]+)\]\s)?(.+)/', $message, $matches)) {

            if(isset($options['raw']) && $options['raw'])
            {
                $serialized['raw'] = $matches[0];
            }

            if(!empty($matches[2])){
                $serialized['exception'] = $matches[2];
            }

            $serialized['message'] = $matches[3];
        }

        if (isset($options['attributes']) && $options['attributes']) {
            //Sets the exception attributes
            if (preg_match('/Exception Attributes:\s((.(?!Request URL|Referer URL|Stack Trace|Trace))+)/is', $message, $matches)) {
                $serialized['attributes'] = $matches[1];
            }
        }

        if (isset($options['request']) && $options['request']) {
            //Sets the request URL
            if(preg_match('/^Request URL:\s(.+)$/mi', $message, $matches)) {
                $serialized['request'] = $matches[1];
            }
        }

        if (isset($options['referer']) && $options['referer']) {
            //Sets the referer URL
            if (preg_match('/^Referer URL:\s(.+)$/mi', $message, $matches)) {
                $serialized['referer'] = $matches[1];
            }
        }

        if(isset($options['trace']) && $options['trace']){
            //Sets the trace
            if(preg_match('/(Stack )?Trace:\n(.+)$/is', $message, $matches)) {
                $serialized['trace'] = $matches[2];
            }
        }


        return $serialized;

    }

}



