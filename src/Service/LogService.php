<?php
namespace App\Service;

use App\Service\AbstractService;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use Cake\Log\Log;
use Cake\ORM\TableRegistry;

use Cake\I18n\FrozenTime;
use Cake\Utility\Inflector;
use Cake\Utility\Hash;

use Laminas\Diactoros\Request\ArraySerializer as ReqArray;
use Laminas\Diactoros\Response\ArraySerializer as ResArray;
use Laminas\Diactoros\Request\Serializer as ReqSerializer;
use Laminas\Diactoros\Response\Serializer as ResSerializer;

use Cake\Routing\Router;

class LogService extends AbstractService {


    public function saveRequest(ServerRequestInterface $req,
        ResponseInterface $res, $responseTime) {

        $req_uri = $req->getUri();
        $operationId = $req->getParam('prefix') .':'. $req->getParam('controller').':'.$req->getParam('action');

        try {
            $requestsTable = TableRegistry::getTableLocator()->get('Requests');

            $now = FrozenTime::now()->toUnixString();
            $now = FrozenTime::createFromTimestamp($now, 'America/Sao_Paulo');
            $now = $now->i18nFormat('yyyy-MM-dd HH:mm:ss');

            $data = [
                'timestamp'=> $now,
                'request' => (new \Laminas\Diactoros\Response\JsonResponse(json_encode(ReqArray::toArray($req))))->getPayload(),
                'request_raw' => ReqSerializer::toString($req),
                'request_body' => (string)$req->getBody(),
                'request_method' => $req->getEnv('ORIGINAL_REQUEST_METHOD'),
                'request_clength' => !empty((string)$req->getBody()) ? mb_strlen(json_encode((string)$req->getBody(), JSON_NUMERIC_CHECK), '8bit') : 0,
                'request_host' => $req->getEnv('SERVER_NAME').':'.$req->getEnv('SERVER_PORT'),
                'request_route_path' => $req_uri->getPath(),
                'request_route_url' => (string)$req_uri,
                'request_scheme' => $req->scheme(),
                'response' => (new \Laminas\Diactoros\Response\JsonResponse(json_encode(ResArray::toArray($res))))->getPayload(),
                'response_raw' => ResSerializer::toString($res),
                'response_body' => !empty((string)$res->getBody()) ? json_encode(json_decode((string)$res->getBody())) : '',
                'response_code' => $res->getStatusCode(),
                'response_phrase' => $res->getReasonPhrase(),
                'response_length' =>  is_null($res->getBody()->getSize()) ? 0 : $res->getBody()->getSize(),
                'response_protocol' => $res->getProtocolVersion(),
                'api_operation_id' => $operationId,
                'api_operation' => $req->getEnv('ORIGINAL_REQUEST_METHOD'),
                'api_path' => $req_uri->getPath(),
                'startts' => $req->getEnv('REQUEST_TIME_FLOAT'),
                'endts' => 0,
                'response_time' => $responseTime, // $res->getHeaderLine('X-Response-Time')
                'api_query' => $req_uri->getQuery(),
                'api_params_id' => '',
                'api_user_ip' => $req->clientIp(),
                'api_user_ua' => $req->getEnv('HTTP_USER_AGENT')
            ];

            $requestsData = $requestsTable->newEntity($data);

            if ($requestsTable->save($requestsData)) {
                return true;
            }

        } catch (\Exception $e) {

            Log::error('LogService :: '.print_r($e, 1));
            return false;
        }

    }




}
