<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Core\Configure;

// Middleware
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use App\Middleware\LogsMiddleware;
use App\Middleware\CorsMiddleware;
use App\Middleware\ResponseTimeMiddleware;

return static function (RouteBuilder $routes) {
    /*
     * The default class to use for all routes
     *
     * The following route classes are supplied with CakePHP and are appropriate
     * to set as the default:
     *
     * - Route
     * - InflectedRoute
     * - DashedRoute
     *
     * If no call is made to `Router::defaultRouteClass()`, the class used is
     * `Route` (`Cake\Routing\Route\Route`)
     *
     * Note that `Route` does not do any inflections on URLs which will result in
     * inconsistently cased URLs when used with `{plugin}`, `{controller}` and
     * `{action}` markers.
     */
    $routes->setRouteClass(DashedRoute::class);

    /**
     * ENDPOINTS RELATION
     *
     * /api/
     *
     *   AUTH   --------------------------------------------------------------------------------
     *
     *   auth/sign-in                     | POST | autenticar um usuário
     *   auth/sign-out                    | POST | remover token de autenticação
     *
     *   auth/sign-up                     | POST | registro de um usuário
     *   auth/sign-up/about               | POST | obter info sobre o registro
     *   auth/sign-up/terms               | POST | obter info sobre os termos de registro
     *   auth/account/refresh-token       | POST | atualizar token de autenticação
     *   auth/account/verify-email        | POST | verifica se email existe
     *   auth/account/forgot-password     | POST |
     *   auth/account/reset-password      | POST |
     *   auth/account/info                | GET  | Obter informações de uma conta
     *
     *   BACKOFFICE   --------------------------------------------------------------------------
     *
     *   backoffice/home                                       | GET |
     *   backoffice/notifications                              | GET |
     *
     *   backoffice/local/countries                            | GET | Obter lista de Países
     *   backoffice/local/countries/{`id`}                     | GET | Obter um país por id
     *   backoffice/local/states                               | GET | Obter lista de Estados
     *   backoffice/local/states/{`id`}                        | GET | Obter um estado por id
     *   backoffice/local/states?q={`argumento`}               | GET | Ira buscar por nome ou descrição, usado para campos autocompletar
     *   backoffice/local/cities                               | GET | Obter cidades
     *   backoffice/local/cities/{`id`}                        | GET | Obter uma cidade
     *   backoffice/local/cities?state_id={`id`}               | GET | Obter cidades de um estado
     *   backoffice/local/cities?q={`argumento`}               | GET | Obter cidades por nome ou descricao de acordo com o argumento preenchido
     *   backoffice/local/adresses?postal_code={`cep`}         | GET | Obter informações de um cep
     *   backoffice/local/adresses?public_place={`Av Paulista`}| GET | Obter informações de um endereço
     *   backoffice/local/adresses?lat={`lat`}&long={`long`}   | GET | Obter informações de um endereço por lat long
     *
     *
     *   APP   ---------------------------------------------------------------------------------
     *
     *   app/home
     *   app/notifications                              | GET |
     *
     *   app/user/profile
     *
     *   app/account/info
     *
     *   app/local/countries                                   | GET | Obter lista de Países
     *   app/local/countries/{`id`}                            | GET | Obter um país por id
     *   app/local/states                                      | GET | Obter lista de Estados
     *   app/local/states/{`id`}                               | GET | Obter um estado por id
     *   app/local/states?q={`argumento`}                      | GET | Ira buscar por nome ou descrição, usado para campos autocompletar
     *   app/local/cities                                      | GET | Obter cidades
     *   app/local/cities/{`id`}                               | GET | Obter uma cidade
     *   app/local/cities?state_id={`id`}                      | GET | Obter cidades de um estado
     *   app/local/cities?q={`argumento`}                      | GET | Obter cidades por nome ou descricao de acordo com o argumento preenchido
     *   app/local/adresses?postal_code={`cep`}                | GET | Obter informações de um cep
     *   app/local/adresses?public_place={`Av Paulista`}       | GET | Obter informações de um endereço
     *   app/local/adresses?lat={`lat`}&long={`long`}          | GET | Obter informações de um endereço por lat long
     *
     *   DEVOPS   ------------------------------------------------------------------------------
     *
     *   devops/home
     *   devops/notifications                              | GET |
     *   devops/logs
     *
     */

    $routes->scope('/'.Configure::read('api.namespace'), function (RouteBuilder $builder) {

        $builder->setExtensions(['json']);

        /**
         * Middlewares
         */
        $builder->registerMiddleware('csrf', new CsrfProtectionMiddleware(
            Configure::read('api.auth.middleware.csrf.options')
        ));

        $builder->registerMiddleware('logs', new LogsMiddleware());

        $builder->registerMiddleware('cors', new CorsMiddleware());

        $builder->registerMiddleware('ResponseTime', new ResponseTimeMiddleware());


         /**
         * Auth
         */
        $builder->prefix('Auth', function (RouteBuilder $routes) {

            /**
             * Auth.Middlewares
             */

            $routes->applyMiddleware('ResponseTime');

            if(Configure::read('api.auth.middleware.csrf.enabled')){
                $routes->applyMiddleware('csrf');
            }

            if(Configure::read('api.auth.middleware.logs.enabled')){
                $routes->applyMiddleware('logs');
            }

            if(Configure::read('api.auth.middleware.cors.enabled')){
                 $routes->applyMiddleware('cors');
            }

            // $routes->applyMiddleware('ResponseTime');


            /**
             * Auth.Routes
             */
            // api/app/  TODO: security




            // api/auth/register
            $routes->connect(
                '/register',
                ['controller' => 'SignUp', 'action' => 'register']
            )->setMethods(['POST']);

            // api/auth/sign-in
            $routes->connect(
                '/sign-in',
                ['controller' => 'SignIn', 'action' => 'login']
            )->setMethods(['POST']);

            // api/auth/sign-out
            $routes->connect(
                '/sign-out',
                ['controller' => 'SignIn', 'action' => 'logout']
            )->setMethods(['POST']);

            // api/auth/account/refresh-token
            $routes->connect(
                '/account/refresh-token',
                ['controller' => 'AccountUser', 'action' => 'refreshToken']
            )->setMethods(['POST']);

            // api/auth/account/verify-email
            $routes->connect(
                '/account/verify-email',
                ['controller' => 'AccountUser', 'action' => 'verifyEmail']
            )->setMethods(['POST']);

            // api/auth/account/forgot-password
            $routes->connect(
                '/account/forgot-password',
                ['controller' => 'AccountUser', 'action' => 'forgotPassword']
            )->setMethods(['POST']);

            // api/auth/account/reset-password
            $routes->connect(
                '/account/reset-password',
                ['controller' => 'AccountUser', 'action' => 'resetPassword']
            )->setMethods(['POST']);

            // api/auth/account/info
            $routes->connect(
                '/account/info',
                ['controller' => 'AccountUser', 'action' => 'info']
            )->setMethods(['GET']);

            $routes->fallbacks(DashedRoute::class);
        });



        /**
         * Backoffice Routes
         */
        $builder->prefix('Backoffice', function (RouteBuilder $routes) {

            /**
             * Backoffice.Middlewares
             */

            $routes->applyMiddleware('ResponseTime');

            if(Configure::read('api.backoffice.middleware.csrf.enabled')){
                $routes->applyMiddleware('csrf');
            }

            if(Configure::read('api.backoffice.middleware.logs.enabled')){
                $routes->applyMiddleware('logs');
            }

            if(Configure::read('api.backoffice.middleware.cors.enabled')){
                $routes->applyMiddleware('cors');
            }


            /**
             * Backoffice.Routes
             */

            // api/backoffice/  TODO: security

            // api/backoffice/home
            $routes->connect(
                '/home',
                [ 'controller' => 'Home', 'action' => 'index']
            )->setMethods(['GET']);

            // api/backoffice/notifications
            $routes->connect(
                '/notifications',
                [ 'controller' => 'Notifications', 'action' => 'index']
            )->setMethods(['GET'])
            ->setPatterns(['q' => '[a-z]']);



            // api/backoffice/local/countries
            $routes->connect(
                '/local/countries',
                [ 'controller' => 'LocalCountries', 'action' => 'index']
            )->setMethods(['GET'])
             ->setPatterns(['q' => '[a-z]']);

            // api/backoffice/local/countries/{id}
            $routes->connect(
                '/local/countries/:id',
                [ 'controller' => 'LocalCountries', 'action' => 'view']
            )->setMethods(['GET'])
            ->setPatterns(['id' => '\d+'])
            ->setPass(['id']);

            // api/backoffice/local/states
            $routes->connect(
                '/local/states',
                [ 'controller' => 'LocalStates', 'action' => 'index']
            )->setMethods(['GET'])
            ->setPatterns(['q' => '[a-z]']);

            // api/backoffice/local/states/{id}
            $routes->connect(
                '/local/states/:id',
                [ 'controller' => 'LocalStates', 'action' => 'view']
            )->setMethods(['GET'])
            ->setPatterns(['id' => '\d+'])
            ->setPass(['id']);


        });




        /**
         * App
         */
        $builder->prefix('App', function (RouteBuilder $routes) {

            /**
             * App.Middlewares
             */

             $routes->applyMiddleware('ResponseTime');

            if(Configure::read('api.app.middleware.csrf.enabled')){
                $routes->applyMiddleware('csrf');
            }

            if(Configure::read('api.app.middleware.logs.enabled')){
                $routes->applyMiddleware('logs');
            }

            if(Configure::read('api.app.middleware.cors.enabled')){
                $routes->applyMiddleware('cors');
            }

            // api/app/  TODO: security

            // api/app/home
            $routes->connect(
                '/home',
                ['controller' => 'Home', 'action' => 'index'],
                ['_name' => 'Início']
            )->setMethods(['GET']);

            // api/app/account/info
            $routes->connect(
                '/account/info',
                ['controller' => 'AccountUser', 'action' => 'info']
            )->setMethods(['GET']);

            $routes->fallbacks(DashedRoute::class);
        });





        /**
         * Devops
         */
        $builder->prefix('Devops', function (RouteBuilder $routes) {

            /**
             * Devops.Middlewares
             */

            $routes->applyMiddleware('ResponseTime');

            if(Configure::read('api.devops.middleware.csrf.enabled')){
                $routes->applyMiddleware('csrf');
            }

            if(Configure::read('api.devops.middleware.logs.enabled')){
                $routes->applyMiddleware('logs');
            }

            if(Configure::read('api.devops.middleware.cors.enabled')){
                $routes->applyMiddleware('cors');
            }


            /**
             * Devops.Routes
             */

            // api/devops/  TODO: security

            // api/devops/home
            $routes->connect(
                '/home',
                [ 'controller' => 'Home', 'action' => 'index']
            )->setMethods(['GET']);

            // api/devops/notifications
            $routes->connect(
                '/notifications',
                [ 'controller' => 'Notifications', 'action' => 'index']
            )->setMethods(['GET'])
            ->setPatterns(['q' => '[a-z]']);

            // api/devops/ack
            $routes->connect('/ack',
                ['controller' => 'Healthcheck', 'action' => 'index']
            )->setMethods(['GET']);

            // api/devops/logs
            $routes->connect(
                '/logs',
                [ 'controller' => 'Logs', 'action' => 'index']
            )->setMethods(['GET'])
            ->setPatterns(['q' => '[a-z]']);

            /// TESTES - REMOVER REMOVER REMOVER REMOVER
            $routes->connect('/dev',
                ['controller' => 'Develop', 'action' => 'index']
            );
        });


        /**
         * Sakila Example App
         */
        $builder->prefix('Sakila', function (RouteBuilder $routes) {

            /**
             * Sakila.Middlewares
             */

            $routes->applyMiddleware('ResponseTime');

            // if(Configure::read('api.devops.middleware.csrf.enabled')){
            //     $routes->applyMiddleware('csrf');
            // }

            // if(Configure::read('api.devops.middleware.logs.enabled')){
            //     $routes->applyMiddleware('logs');
            // }

            // if(Configure::read('api.devops.middleware.cors.enabled')){
            //     $routes->applyMiddleware('cors');
            // }


            /**
             * Sakila.Routes
             */

            // api/sakila/  TODO: security

            // GET api/sakila/actors
            $routes->connect(
                '/actors',
                ['controller' => 'Actors', 'action' => 'index']
            )->setMethods(['GET']);

            // GET api/sakila/actors/:id
            $routes->connect(
                '/actors/{id}',
                ['controller' => 'Actors', 'action' => 'view'],
                ['id' => '\d+', 'pass' => ['id']]
            )->setMethods(['GET']);

            // POST api/sakila/actors
            $routes->connect(
                '/actors',
                ['controller' => 'Actors', 'action' => 'add']
            )->setMethods(['POST']);

            // PUT api/sakila/actors/:id
            $routes->connect(
                '/actors/{id}',
                ['controller' => 'Actors', 'action' => 'edit'],
                ['id' => '\d+', 'pass' => ['id']]
            )->setMethods(['PUT']);

            // DELETE api/sakila/actors/:id
            $routes->connect(
                '/actors/{id}',
                ['controller' => 'Actors', 'action' => 'delete'],
                ['id' => '\d+', 'pass' => ['id']]
            )->setMethods(['DELETE']);




            // // api/sakila/home
            // $routes->connect(
            //     '/actors',
            //     [ 'controller' => 'Home', 'action' => 'index']
            // )->setMethods(['GET']);

            // // api/sakila/notifications
            // $routes->connect(
            //     '/notifications',
            //     [ 'controller' => 'Notifications', 'action' => 'index']
            // )->setMethods(['GET'])
            // ->setPatterns(['q' => '[a-z]']);

            // // api/sakila/ack
            // $routes->connect('/ack',
            //     ['controller' => 'Healthcheck', 'action' => 'index']
            // )->setMethods(['GET']);

            // // api/sakila/logs
            // $routes->connect(
            //     '/logs',
            //     [ 'controller' => 'Logs', 'action' => 'index']
            // )->setMethods(['GET'])
            // ->setPatterns(['q' => '[a-z]']);

            // /// TESTES - REMOVER REMOVER REMOVER REMOVER
            // $routes->connect('/dev',
            //     ['controller' => 'Develop', 'action' => 'index']
            // );
        });

        /*
         * Here, we are connecting '/' (base path) to a controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, templates/Pages/home.php)...
         */
        $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

        /*
         * ...and connect the rest of 'Pages' controller's URLs.
         */
        // $builder->connect('/pages/*', 'Pages::display');

        /*
         * Connect catchall routes for all controllers.
         *
         * The `fallbacks` method is a shortcut for
         *
         * ```
         * $builder->connect('/{controller}', ['action' => 'index']);
         * $builder->connect('/{controller}/{action}/*', []);
         * ```
         *
         * You can remove these routes once you've connected the
         * routes you want in your application.
         */
        $builder->fallbacks();
    });

    /*
     * If you need a different set of middleware or none at all,
     * open new scope and define routes there.
     *
     * ```
     * $routes->scope('/api', function (RouteBuilder $builder) {
     *     // No $builder->applyMiddleware() here.
     *
     *     // Parse specified extensions from URLs
     *     // $builder->setExtensions(['json', 'xml']);
     *
     *     // Connect API actions here.
     * });
     * ```
     */
};
