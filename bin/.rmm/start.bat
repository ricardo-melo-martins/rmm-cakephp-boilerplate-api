REM Script de inicializacao desenvolvimento para ambiente Windows
	IF EXIST ".\vendor" (
		echo ".\vendor exists"
		START cmd.exe /k "php bin/cake.php server -H 127.0.0.1 -p 8888"

		IF EXIST "%ProgramFiles(x86)%\Google\Chrome\Application" (
			START chrome.exe 127.0.0.1:8888 -incognito
		) ELSE (
			echo "Chrome nao encontrado no sistema"
		)

	) ELSE (
		START cmd.exe /k "composer install"
		setlocal EnableDelayedExpansion
		if "!cmdcmdline!" neq "!cmdcmdline:%comspec%=!" ( pause>nul )
	)

exit
